package br.com.papodecafeteria.actions;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import br.com.papodecafeteria.dao.jpa.UserDaoJpa;
import br.com.papodecafeteria.dao.model.UserJPA;
import br.com.papodecafeteria.model.UserBean;

public class SaveUserAction extends Action {
	
	private static Logger l = Logger.getLogger(SaveUserAction.class.getName());
	
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	List<UserJPA> lstUserJPA = null;
    	try{
    		UserJPA userJPA = ((UserBean) form).exportToUserJPA();
    		userJPA.setId(Integer.parseInt(request.getParameter("id")));
    		UserDaoJpa.update(userJPA);
    		
    		lstUserJPA = UserDaoJpa.getAllRecords();
    		request.setAttribute("lstUserJPA", lstUserJPA);
    	} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
    	return mapping.findForward("viewusers");
	}
}
