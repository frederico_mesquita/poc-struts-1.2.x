package br.com.papodecafeteria.actions;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import br.com.papodecafeteria.dao.jpa.UserDaoJpa;
import br.com.papodecafeteria.dao.model.UserJPA;

public class DeleteUserAction extends Action {
	
	private static Logger l = Logger.getLogger(DeleteUserAction.class.getName());
	
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	List<UserJPA> lstUserJPA = null;
    	try{
    		UserJPA userJPA = new UserJPA();
    		userJPA.setId(Integer.parseInt(request.getParameter("id"))); 
    		UserDaoJpa.delete(userJPA);
    		
    		lstUserJPA = UserDaoJpa.getAllRecords();
    		request.setAttribute("lstUserJPA", lstUserJPA);
    	} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
    	return mapping.findForward("viewusers");
	}
}
