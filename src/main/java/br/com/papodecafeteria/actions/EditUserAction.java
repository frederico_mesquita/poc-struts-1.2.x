package br.com.papodecafeteria.actions;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import br.com.papodecafeteria.dao.jpa.UserDaoJpa;
import br.com.papodecafeteria.dao.model.UserJPA;

public class EditUserAction extends Action {
	
	private static Logger l = Logger.getLogger(EditUserAction.class.getName());
	
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	UserJPA userJPA = null;
    	try{
    		userJPA = UserDaoJpa.getRecordById(Integer.parseInt(request.getParameter("id")));

    		request.setAttribute("userJPA", userJPA);
    	} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
    	return mapping.findForward("editform");
	}
}
