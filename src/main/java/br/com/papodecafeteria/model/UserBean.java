package br.com.papodecafeteria.model;

import org.apache.struts.action.ActionForm;

import br.com.papodecafeteria.dao.model.UserJPA;

public class UserBean extends ActionForm {
	private static final long serialVersionUID = 1L;
	
	private int id = 0;
	private String name = "";
	private String password = "";
	private String email = "";
	private String sex = "";
	private String country = "";
	
	public UserBean(){}
	
	public UserJPA exportToUserJPA(){
		UserJPA userJPA = new UserJPA();
		
		userJPA.setName(name);
		userJPA.setPassword(password);
		userJPA.setEmail(email);
		userJPA.setSex(sex);
		userJPA.setCountry(country);
		
		return userJPA;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getId() {
		return id;
	}
}
