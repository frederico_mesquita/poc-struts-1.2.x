<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script>
		$(document).ready(function(){
			$(".restore").fadeOut();
		    $(".link").click(function(){
		        $("#home").fadeOut();
		    });
		});
		
		$(document).ready(function(){
			$(".line").click(function(){
		        $(this).slideUp();
		        $(".restore").fadeIn();
		    });
			$(".restore").click(function(){
				$(".line").slideDown();
		        $(".restore").fadeOut();
		    });
		});
		</script>
		<title>View Users</title>
	</head>
	<body>
		<div align="center" id="viewusers">
			<h1>List Users</h1>
			<logic:notEmpty name="lstUserJPA">
				<table border="1" width="90%" cellpadding="10">
					<thead>
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>Email</th>
							<th>Sex</th>
							<th>Country</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
					</thead>
					<logic:iterate name="lstUserJPA" id="user">
						<tr id="line" class="line">
							<td align="center"><bean:write name="user" property="id" /></td>
							<td id="name"><bean:write name="user" property="name" /></td> 
							<td id="email"><bean:write name="user" property="email" /></td>
							<td id="sex" align="center"><bean:write name="user" property="sex" /></td>
							<td id="country"><bean:write name="user" property="country" /></td> 
							<td id="edit" align="center"><html:link action="edituser" paramId="id" paramName="user" paramProperty="id">Edit</html:link></td>
							<td id="delete" align="center"><html:link action="deleteuser" paramId="id" paramName="user" paramProperty="id">Delete</html:link></td>
						</tr>
					</logic:iterate>
				</table>  
			</logic:notEmpty>  
			<br/>
			<p>
				<html:link href="../struts/crud/adduserform.jsp">Add User</html:link>
				<html:link href="../struts/index.jsp">Home</html:link>
			</p>
			<p class="restore">Click me to restores table�s lines.</p>
		</div>
	</body>
</html>