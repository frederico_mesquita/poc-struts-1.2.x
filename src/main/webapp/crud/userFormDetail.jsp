<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>User Form Detail</title>
	</head>
	<body>		
		<jsp:useBean id="pUser" class="br.com.papodecafeteria.dao.model.UserJPA"></jsp:useBean>  
		<jsp:setProperty property="*" name="pUser"/>
		<h1>Add New User - Detail</h1>
		<form id="adduserform" action="adduser.jsp" method="post">
			<input type="hidden" id="name" name="name" value=<jsp:getProperty property="name" name="pUser"/>>
			<input type="hidden" id="password" name="password" value=<jsp:getProperty property="password" name="pUser"/>>
			<input type="hidden" id="email" name="email" value=<jsp:getProperty property="email" name="pUser"/>>
			<input type="hidden" id="sex" name="sex" value=<jsp:getProperty property="sex" name="pUser"/>>
			<input type="hidden" id="country" name="country" value=<jsp:getProperty property="country" name="pUser"/>>
			<table>  
				<tr><td>Name:</td><td><jsp:getProperty property="name" name="pUser"/></td></tr>  
				<tr><td>Password:</td><td><jsp:getProperty property="password" name="pUser"/></td></tr>  
				<tr><td>Email:</td><td><jsp:getProperty property="email" name="pUser"/></td></tr>  
				<tr>
					<td>Sex:</td>
					<td><jsp:getProperty property="sex" name="pUser"/></td>
				</tr>  
				<tr>
					<td>Country:</td>
					<td><jsp:getProperty property="country" name="pUser"/></td>
				</tr>
				<tr align="center">
					<td><input id="adduser" type="submit" value="Add User"/></td>
					<td><button id="cancel" onclick="location.href='../index.jsp'" type="button">Cancel</button>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>