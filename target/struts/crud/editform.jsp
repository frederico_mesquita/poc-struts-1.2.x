<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Edit Form</title>
	</head>
	<body>
		<div align="center" id="edituser">
			<h1>Edit Form</h1>
			<form id="edituserform" action="saveuser.do" method="post">  
				<input type="hidden" name="id" value="<bean:write name="userJPA" property="id" />"/>  
				<table>  
					<tr><td>Name:</td><td><input type="text" id="name" name="name" value="<bean:write name="userJPA" property="name" />"/></td></tr>  
					<tr><td>Email:</td><td><input type="email" id="email" name="email" value="<bean:write name="userJPA" property="email" />"/></td></tr>  
					<tr>
						<td>Sex:</td>
						<td>
							<logic:match name="userJPA" property="sex" value="female">
								<input type="radio" name="sex" id="female" value="female" checked="checked" />Female
								<input type="radio" name="sex" id="male" value="male" />Male
							</logic:match>
							<logic:notMatch name="userJPA" property="sex" value="female">
								<input type="radio" name="sex" id="female" value="female" />Female
								<input type="radio" name="sex" id="male" value="male" checked="checked" />Male
							</logic:notMatch>
						</td>
					</tr>  
					<tr>
						<td>Country:</td>
						<td>
							<select id="country" name="country" style="width:155px">  
								<logic:match name="userJPA" property="country" value="Peru">
									<option selected="selected">Peru</option>
								</logic:match>
								<logic:notMatch name="userJPA" property="country" value="Peru">
									<option>Peru</option>
								</logic:notMatch>
								
								<logic:match name="userJPA" property="country" value="Argentina">
									<option selected="selected">Argentina</option>
								</logic:match>
								<logic:notMatch name="userJPA" property="country" value="Argentina">
									<option>Argentina</option>
								</logic:notMatch>
								
								<logic:match name="userJPA" property="country" value="Bolivia">
									<option selected="selected">Bolivia</option>
								</logic:match>
								<logic:notMatch name="userJPA" property="country" value="Bolivia">
									<option>Bolivia</option>
								</logic:notMatch>
								
								<logic:match name="userJPA" property="country" value="Uruguay">
									<option selected="selected">Uruguay</option>
								</logic:match>
								<logic:notMatch name="userJPA" property="country"  value="Uruguay">
									<option>Uruguay</option>
								</logic:notMatch>
								
								<logic:match name="userJPA" property="country" value="Chile">
									<option selected="selected">Chile</option>
								</logic:match>
								<logic:notMatch name="userJPA" property="country" value="Chile">
									<option>Chile</option>
								</logic:notMatch>
								
								<logic:match name="userJPA" property="country" value="Brazil">
									<option selected="selected">Brazil</option>
								</logic:match>
								<logic:notMatch name="userJPA" property="country" value="Brazil">
									<option>Brazil</option>
								</logic:notMatch>
								
								<logic:match name="userJPA" property="country" value="Other">
									<option selected="selected">Other</option>
								</logic:match>
								<logic:notMatch name="userJPA" property="country" value="Other">
									<option>Other</option>
								</logic:notMatch>  
							</select>
						</td>
					</tr>
					<tr align="center">
						<td><input id="edituser" type="submit" value="Save User"/></td>
						<td><html:link href="../struts/index.jsp">Home</html:link></td>
					</tr>  
				</table>  
			</form>
		</div>
	</body>
</html>